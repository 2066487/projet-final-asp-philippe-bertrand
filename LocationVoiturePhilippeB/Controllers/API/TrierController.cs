﻿using LocationVoiturePhilippeB.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;

namespace LocationVoiturePhilippeB.Controllers.API
{
    /// <summary>
    /// Contrôleur qui permet la conssomation de l'API
    /// - Une certaine partie du code est inspiré d'une démo d'utilisation d'API de Dalicia Bouallouche
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TrierController : ControllerBase
    {
        private readonly IClientRepository _clientRepository;

        public TrierController(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }
        /// <summary>
        /// Méthode d'action qui récupère les clients de la table sous forme de liste
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public OkObjectResult GetClients()
        {
            List<Client> clients = _clientRepository.ListeClients;

            return Ok(clients);
        }
        /// <summary>
        /// Méthode d'action qui récupère un client selon son ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult GetClient(int id) 
        {
            Client client = _clientRepository.GetClientById(id);

            if (client == null)
            {
                return NotFound();
            }
            return new JsonResult(client) { StatusCode = 200 };
        }
        /// <summary>
        /// Méthode d'action qui trie les clients selon un critère sélectionné par l'utilisateur :
        /// - Prénom (ordre,désordre)
        /// - Nom (ordre,désordre)
        /// - Id (ordre)
        /// </summary>
        /// <param name="ordre"> Paramètre entrée dans le select list dans le formulaire directement</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult GetClients([FromForm] string ordre)
        {
            List<Client> clients = _clientRepository.ListeClients;

            switch (ordre)
            {
                case "Prénom Ordre":
                    clients = clients.OrderBy(c => c.Prenom).ToList();
                    break;
                case "Prénom Non-Ordre":
                    clients = clients.OrderByDescending(c => c.Prenom).ToList();
                    break;
                case "Nom Ordre":
                    clients = clients.OrderBy(c => c.Nom).ToList();
                    break;
                case "Nom Non-Ordre":
                    clients = clients.OrderByDescending(c => c.Nom).ToList();
                    break;
                case "ID":
                    clients = clients.OrderBy(c => c.Id).ToList();
                    break;
                // Ajoutez d'autres cas selon vos besoins de tri
                default:
                    return BadRequest("Ordre de tri non valide");
            }

            // Chargement du schéma JSON à partir du fichier clients-json-schema.json dans wwwroot\SchemasJson
            string fichierJson = System.IO.File.ReadAllText(@"wwwroot/SchemasJson/clients-json-schema.json");
            JSchema schemaJsonClients = JSchema.Parse(fichierJson);

            // Conversion de la liste de clients en objet JSON
            JArray clientsJson = JArray.FromObject(clients);

            // Validation des données JSON clientsJson par rapport au schéma JSON schemaJsonClients
            bool clientsJsonValide = clientsJson.IsValid(schemaJsonClients, out IList<string> erreurs);

            if (!clientsJsonValide)
            {
                return BadRequest(string.Join(", ", erreurs));
            }

            return Ok(clients);
        }
    }
}
