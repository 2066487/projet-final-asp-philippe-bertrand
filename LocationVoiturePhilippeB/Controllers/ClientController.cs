﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using static System.Net.Mime.MediaTypeNames;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using LocationVoiturePhilippeB.Models;

namespace LocationVoiturePhilippeB.Controllers
{
    /// <summary>
    /// Contrôleur pour la gestion complète des clients
    /// </summary>
    [Authorize(Roles = "Admin, Client")]
    public class ClientController : Controller
    {
        private IClientRepository _clientRepository;
        private IWebHostEnvironment _hostingEnvironment;
        public ClientController(IClientRepository clientRepository, IWebHostEnvironment webHostEnvironment)
        {
            _clientRepository = clientRepository;
            _hostingEnvironment = webHostEnvironment;
        }
        /// <summary>
        /// Retourne la liste des clients
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Client, Admin")]
        public ViewResult ListeClients()
        {
            return View(_clientRepository.ListeClients);
        }
        /// <summary>
        /// Affichage de la page d'ajout d'un client
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ViewResult Ajouter()
        {
            return View();
        }
        /// <summary>
        /// Affichage de la page de modification d'un client
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ViewResult Modifier(int id)
        {
            Client client = _clientRepository.GetClientById(id);
            return View(client);
        }
        /// <summary>
        /// Méthode d'action qui permet la suppression d'un client de la liste
        /// </summary>
        /// <param name="id"> Correspond au client sélectionné</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        public RedirectToActionResult Supprimer(int id)
        {
            _clientRepository.Supprimer(id);
            return RedirectToAction(nameof(ListeClients));
        }
        /// <summary>
        /// Méthode d'action qui permet l'ajout d'un client dans la liste
        /// - Redirection vers la vue des clients (ListeClients)
        /// - Le code de téléversement sécuritaire est pris d'une démo de Dalicia Bouallouche
        /// - Utilisation de la validation côté serveur 
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        [HttpPost]
        public RedirectToActionResult AjouterClient(Client client)
        {
            if (client.Photo != null)
            {
                string nomImage = Path.GetFileName(client.Photo.FileName);
                string extensionImage = Path.GetExtension(nomImage);

                var repertoireTeleversements = Path.Combine(_hostingEnvironment.ContentRootPath, "Televersements\\Clients");

                Directory.CreateDirectory(repertoireTeleversements);

                nomImage = Guid.NewGuid().ToString() + extensionImage;

                var cheminImage = Path.Combine(repertoireTeleversements, nomImage);
                client.urlPhoto = cheminImage;


                // Enregistrer l'image sur le disque
                using (var stream = new FileStream(cheminImage, FileMode.Create))
                {
                    client.Photo.CopyTo(stream);
                }
            }
            if (ModelState.IsValid)
            {
                _clientRepository.Ajouter(client);
                return RedirectToAction(nameof(ListeClients));
            }
            
            return RedirectToAction(nameof(Ajouter));
        }
        /// <summary>
        /// Méthode d'action qui permet la modification d'un client dans la liste
        /// - Redirection vers la vue des clients (ListeClients)
        /// - Le code de téléversement sécuritaire est pris d'une démo de Dalicia Bouallouche
        /// - Utilisation de la validation côté serveur 
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        [HttpPost]
        public RedirectToActionResult ModifierClient(Client client)
        {
            if (client.Photo != null)
            {
                string nomImage = Path.GetFileName(client.Photo.FileName);
                string extensionImage = Path.GetExtension(nomImage);

                var repertoireTeleversements = Path.Combine(_hostingEnvironment.ContentRootPath, "Televersements\\Clients");

                Directory.CreateDirectory(repertoireTeleversements);

                nomImage = Guid.NewGuid().ToString() + extensionImage;

                var cheminImage = Path.Combine(repertoireTeleversements, nomImage);
                client.urlPhoto = cheminImage;


                // Enregistrer l'image sur le disque
                using (var stream = new FileStream(cheminImage, FileMode.Create))
                {
                    client.Photo.CopyTo(stream);
                }
            }
            if (ModelState.IsValid) 
            {
                _clientRepository.Modifier(client);
                return RedirectToAction(nameof(ListeClients));
            }
            
            return RedirectToAction(nameof(Modifier));
        }
       
    }
}
