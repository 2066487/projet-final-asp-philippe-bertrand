﻿using LocationVoiturePhilippeB.ViewModels;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace LocationVoiturePhilippeB.Controllers
{
    /// <summary>
    /// Contrôleur qui permet d'afficher les différents messages dans la vue d'erreur personnalisée selon le code de statut
    /// Code tiré de la démo des gestions d'exeptions de Dalicia Bouallouche
    /// </summary>
    public class ErreursController : Controller
    {
        [Route("Erreurs/GererErreur{codeEtat}")]
        public ViewResult GererErreur(int codeEtat)
        {
            ErreurViewModel erreurViewModel = new ErreurViewModel
            {
                CodeErreur = codeEtat,
            };
            if (codeEtat == 404)
            {
                erreurViewModel.ErrorMsg = "Désolé, cette page n'existe pas";
            }

            return View("Erreur", erreurViewModel);
        }

        [Route("Erreurs/GererErreurGlobale")]
        public ViewResult GererErreurGlobale()
        {
            IExceptionHandlerPathFeature exceptionDetails = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            string hostingEnv = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            ErreurViewModel erreurViewModel = new ErreurViewModel
            {
                CodeErreur = HttpContext.Response.StatusCode,
                ErrorMsg = "Oops, Une erreur s'est produite durant l'exécution de votre requête!",

                CheminException = exceptionDetails.Path,
                MessageException = exceptionDetails.Error.Message,
                TraceException = exceptionDetails.Error.StackTrace,
                ModeDev = hostingEnv == "Development"
            };

            return View("Erreur", erreurViewModel);
        }
    }
}
