﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LocationVoiturePhilippeB.Controllers
{
    /// <summary>
    /// Controlleur qui permet l'utilisation de fichier en dehors du root
    /// - Code tiré d'une démo de Dalicia Bouallouche
    /// </summary>
    public class FichierController : Controller
    {
        private IWebHostEnvironment _hostingEnvironment;

        /// <summary>
        /// Constructeur du contrôleur de fichiers.
        /// </summary>
        /// <param name="hostingEnvironment">Service IWebHostEnvironment qui 
        ///         donne accès aux informations sur l'environnement d'hébergement 
        ///         de l'application.</param>
        public FichierController(IWebHostEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// Méthode d'action qui permet de lire un fichier en dehors de wwwroot.
        /// Utilisée dans des vues qui affichent des images téléversées et 
        /// stockées en dehors de wwwroot.
        /// </summary>
        /// <param name="nomImage">Le nom de l'image</param>
        /// <param name="cheminDossier"></param>
        /// <returns></returns>
        public FileResult GetImage(string cheminImage)
        {
            if (cheminImage != null)
            {
                cheminImage = Path.Combine(_hostingEnvironment.ContentRootPath, cheminImage);

                var fileStream = new FileStream(cheminImage, FileMode.Open, FileAccess.Read);

                return File(fileStream, "images/jpg");
            }
            else { return null; }
        }
    }
}
