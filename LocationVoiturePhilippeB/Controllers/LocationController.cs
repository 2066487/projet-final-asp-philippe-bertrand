﻿using LocationVoiturePhilippeB.Models;
using LocationVoiturePhilippeB.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace LocationVoiturePhilippeB.Controllers
{
    /// <summary>
    /// Controlleur pour afficher le viewmodel qui contient les locations
    /// </summary>
    public class LocationController : Controller
    {
        private readonly IPanierRepository _panierRepository;
     
        public LocationController(IPanierRepository panier)
        {
            _panierRepository = panier;
        }

        public ViewResult AfficherLocation()
        {
            LocationViewModel model = new LocationViewModel()
            {
                ListeLocations = _panierRepository.GetLocations(),
                Datedebut = DateTime.Now,
                MontantTotal = _panierRepository.MontantTotalPanier()
            };
            return View(model);
        }
    }
}
