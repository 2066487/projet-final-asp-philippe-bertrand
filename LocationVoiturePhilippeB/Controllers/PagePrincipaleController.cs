﻿using LocationVoiturePhilippeB.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace LocationVoiturePhilippeB.Controllers
{
    /// <summary>
    /// Controlleur qui permet l'affichage du ViewModel pour la page principale du site
    /// Le ViewModel trie les voitures en fonction de leur nombre de locations 
    /// </summary>
    public class PagePrincipaleController : Controller
    {
        private readonly PagePrincipaleViewModel _pagePrincipaleViewModel;

        public PagePrincipaleController(PagePrincipaleViewModel pagePrincipaleViewModel)
        {
            _pagePrincipaleViewModel = pagePrincipaleViewModel;
        }

        public ViewResult Index()
        {

            // Vérifier si l'utilisateur est authentifié
            bool estAuthentifie = User.Identity.IsAuthenticated;

            // Création d'un cookie qui vérifie si l'utilisateur est authentifié ou pas
            CookieOptions options = new CookieOptions
            {
                Expires = DateTime.Now.AddMinutes(10) 
            };

            Response.Cookies.Append("Authentifie", estAuthentifie.ToString(), options);

            return View(_pagePrincipaleViewModel);
        }
    }
}
