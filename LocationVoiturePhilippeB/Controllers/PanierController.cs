﻿using LocationVoiturePhilippeB.Models;
using LocationVoiturePhilippeB.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LocationVoiturePhilippeB.Controllers
{
    /// <summary>
    /// Contrôlleur qui permet la gestion complète du panier de locations
    /// </summary>
    [Authorize(Roles = "Admin, Client")]
    public class PanierController : Controller
    {
        private readonly IPanierRepository _panierRepository;
        private readonly IVoitureRepository _voitureRepository;
        private IHttpContextAccessor _contextAccessor;

        /// <summary>
        /// Constructeur du controlleur pour créer une session à chaque utilisation du panier
        /// </summary>
        /// <param name="panier"></param>
        /// <param name="voiture"></param>
        /// <param name="contextAccessor"></param>
        public PanierController(IPanierRepository panier, IVoitureRepository voiture, IHttpContextAccessor contextAccessor)
        {
            _panierRepository = panier;
            _voitureRepository = voiture;
            _contextAccessor = contextAccessor;

            var panierSessionId = _contextAccessor.HttpContext.Session.GetString("PanierSessionId");
            if (string.IsNullOrEmpty(panierSessionId))
            {
                panierSessionId = Guid.NewGuid().ToString();
                _contextAccessor.HttpContext.Session.SetString("PanierSessionId", panierSessionId);
            }
            _panierRepository.SetSessionId(panierSessionId);
            ViewBag.PanierSessionId = panierSessionId;
        }
        /// <summary>
        /// Affichage du panier d'achat dans le ViewModel attribué
        /// - Le montant total est calculé avec une requête LINQ du PanierRepository
        /// </summary>
        /// <returns></returns>
        public ViewResult AfficherPanier()
        {
            PanierViewModel model = new PanierViewModel
            {
                ListeVoituresPanier = _panierRepository.VoiturePanier,
                MontantTotal = _panierRepository.MontantTotalPanier()
            };
            return View(model);
        }
        /// <summary>
        /// Méthode d'action qui permet l'ajout d'une voiture dans le panier
        /// - Appelle de la fonction "AjouterAuPanier" du PanierRepository
        /// </summary>
        /// <param name="vehiculeId"> Récupérer automatiquement quand l'utilisateur appuie sur "Ajouter au panier" dans la vue </param>
        /// <returns></returns>
        public RedirectToActionResult AjouterAuPanier(int vehiculeId)
        {
            Voiture voiture = _voitureRepository.GetVoitureById(vehiculeId);

            _panierRepository.AjouterAuPanier(voiture);

            return RedirectToAction(nameof(AfficherPanier));
        }
        /// <summary>
        /// Méthode d'action qui permet la suppression d'une voiture dans le panier
        /// - Appelle de la fonction "SupprimerDuPanier" du PanierRepository
        /// </summary>
        /// <param name="vehiculeId"></param>
        /// <returns></returns>
        public RedirectToActionResult SupprimerDuPanier(int vehiculeId)
        {
            Voiture voiture = _voitureRepository.GetVoitureById(vehiculeId);

            _panierRepository.SupprimerDuPanier(voiture);

            return RedirectToAction(nameof(AfficherPanier));
        }
        /// <summary>
        /// Méthode d'action qui supprime l'entièreté du contenu du panier
        /// </summary>
        /// <returns></returns>
        public RedirectToActionResult ViderPanier()
        {
            _panierRepository.ViderPanier();

            return RedirectToAction(nameof(AfficherPanier));
        }
    }
}
