﻿using LocationVoiturePhilippeB.Models;
using Microsoft.AspNetCore.Mvc;
using System.Text.RegularExpressions;

namespace LocationVoiturePhilippeB.Controllers.Validation
{
    /// <summary>
    /// Contrôleur qui contient des méthodes d'actions de validation pour les remotes
    /// On appelle les méthodes d'actions directement des les models ex : Client
    /// </summary>
    public class ValidationController : Controller
    {
        private IClientRepository _clientRepository;

        public ValidationController(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }
        /// <summary>
        /// Méthode d'action qui vérifie le format conforme du prénom donné au client :
        /// - Doit commencer par une majuscule suivie de 3 à 30 lettres minuscules
        /// </summary>
        /// <param name="prenom"> Prénom donné par l'utilisateur</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CheckPrenomFormat(string prenom)
        {
            bool isValid = false;

            // Vérifier si le prénom respecte le format spécifié
            if (!string.IsNullOrEmpty(prenom))
            {
                Regex regex = new Regex(@"^[A-Z][a-z]{2,29}$");
                isValid = regex.IsMatch(prenom);
            }
            return Json(isValid);
        }
        /// <summary>
        /// Méthode d'action qui vérifie le format conforme du nom donné au client :
        /// - Doit commencer par une majuscule suivie de 3 à 30 lettres minuscules
        /// </summary>
        /// <param name="nom"> Nom donné par l'utilisateur</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CheckNomFormat(string nom)
        {
            bool isValid = false;

            // Vérifier si le prénom respecte le format spécifié
            if (!string.IsNullOrEmpty(nom))
            {
                // Vérifier si le prénom commence par une lettre majuscule suivie de 3 à 30 lettres minuscules
                Regex regex = new Regex(@"^[A-Z][a-z]{2,29}$");
                isValid = regex.IsMatch(nom);
            }

            return Json(isValid);
        }
        /// <summary>
        /// Méthode d'action qui vérifie si le permis de conduire entré n'existe pas déjà dans la base de données
        /// Appelle de la méthode "IsPermisExists" du repository client
        /// </summary>
        /// <param name="NumeroPermis"> Numéro donné par l'utilisateur</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult PermisExist(string NumeroPermis)
        {
            bool isPermisExists = _clientRepository.IsPermisExists(NumeroPermis);

            if (isPermisExists)
            {
                return Json($"Le numéro de permis {NumeroPermis} existe déjà.");
            }

            return Json(true);
        }
        /// <summary>
        /// Méthode d'action qui vérifie si l'année de fabrication entrée est entre 1970 et l'année actuelle
        /// </summary>
        /// <param name="AnneeFabrication"> Année de fabricaiton donnée par l'utilisateur</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ValiderAnneeFabrication(int AnneeFabrication)
        {
            int anneeActuelle = DateTime.Now.Year;

            if (AnneeFabrication < 1970 || AnneeFabrication > anneeActuelle)
            {
                return Json($"L'année de fabrication doit être entre 1970 et {anneeActuelle}.");
            }

            return Json(true);
        }
    }
}
