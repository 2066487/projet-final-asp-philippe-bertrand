﻿using LocationVoiturePhilippeB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace LocationVoiturePhilippeB.Controllers
{
    /// <summary>
    /// Controlleur qui permet la gestion complète des voitures 
    /// </summary>
    [Authorize(Roles = "Admin, Client")]
    public class VoitureController : Controller
    {
        private IVoitureRepository _voitureRepository;
        private IWebHostEnvironment _webHostEnvironment;

        public VoitureController(IVoitureRepository voitureRepository, IWebHostEnvironment webHostEnvironment)
        {
            _voitureRepository = voitureRepository;
            _webHostEnvironment = webHostEnvironment;
        }
        /// <summary>
        /// Affichage de la liste des voitures disponibles
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Client")]
        public ViewResult ListeVoitures()
        {
            return View(_voitureRepository.ListeVoitures);
        }
        /// <summary>
        /// Affichage de la vue d'ajout d'une voiture
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ViewResult Ajouter()
        {
            return View();
        }
        /// <summary>
        /// Affichage de la vue de modification d'une voiture
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ViewResult Modifier(int id)
        {
            Voiture voiture = _voitureRepository.GetVoitureById(id);
            return View(voiture);
        }
        /// <summary>
        /// Méthode d'action qui permet la suppression d'une voiture dans la base de données
        /// </summary>
        /// <param name="id"> Récupérer automatiquement quand on sélectionne la voiture à supprimer</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        public RedirectToActionResult Supprimer(int id)
        {
            _voitureRepository.Supprimer(id);
            return RedirectToAction(nameof(ListeVoitures));
        }
        /// <summary>
        /// Méthode d'action qui permet l'ajout d'une voiture dans la BD
        /// - Redirection vers la vue des voitures (ListeVoitures)
        /// - Le code de téléversement sécuritaire est pris d'une démo de Dalicia Bouallouche
        /// - Utilisation de la validation côté serveur 
        /// </summary>
        /// <param name="voiture"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public RedirectToActionResult AjouterVoiture(Voiture voiture)
        {
            if (voiture.Image != null)
            {
                string nomImage = Path.GetFileName(voiture.Image.FileName);
                string extensionImage = Path.GetExtension(nomImage);

                var repertoireTeleversements = Path.Combine(_webHostEnvironment.ContentRootPath, "Televersements\\Voitures");

                Directory.CreateDirectory(repertoireTeleversements);

                nomImage = Guid.NewGuid().ToString() + extensionImage;

                var cheminImage = Path.Combine(repertoireTeleversements, nomImage);
                voiture.UrlImage = cheminImage;


                // Enregistrer l'image sur le disque
                using (var stream = new FileStream(cheminImage, FileMode.Create))
                {
                    voiture.Image.CopyTo(stream);
                }
            }
            if (ModelState.IsValid)
            {
                _voitureRepository.Ajouter(voiture);
                return RedirectToAction(nameof(ListeVoitures));
            }
            return RedirectToAction(nameof(Ajouter));
        }
        /// <summary>
        /// Méthode d'action qui permet la modification d'une voiture dans la BD
        /// - Redirection vers la vue des voitures (ListeVoitures)
        /// - Le code de téléversement sécuritaire est pris d'une démo de Dalicia Bouallouche
        /// - Utilisation de la validation côté serveur 
        /// </summary>
        /// <param name="voiture"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public RedirectToActionResult ModifierVoiture(Voiture voiture)
        {
            if (voiture.Image != null)
            {
                string nomImage = Path.GetFileName(voiture.Image.FileName);
                string extensionImage = Path.GetExtension(nomImage);

                var repertoireTeleversements = Path.Combine(_webHostEnvironment.ContentRootPath, "Televersements\\Voitures");

                Directory.CreateDirectory(repertoireTeleversements);

                nomImage = Guid.NewGuid().ToString() + extensionImage;

                var cheminImage = Path.Combine(repertoireTeleversements, nomImage);
                voiture.UrlImage = cheminImage;


                // Enregistrer l'image sur le disque
                using (var stream = new FileStream(cheminImage, FileMode.Create))
                {
                    voiture.Image.CopyTo(stream);
                }
            }
            if (ModelState.IsValid) 
            {
                _voitureRepository.Modifier(voiture);
                return RedirectToAction(nameof(ListeVoitures));
            }
            return RedirectToAction(nameof(Modifier));
        }
    }
}
