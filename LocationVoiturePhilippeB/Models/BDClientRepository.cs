﻿namespace LocationVoiturePhilippeB.Models
{
    public class BDClientRepository : IClientRepository
    {
        private LocationVoitureDbContext _context;

        public BDClientRepository(LocationVoitureDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Récupérer les clients de la base de données sous forme de liste
        /// </summary>
        public List<Client> ListeClients
        {
            get
            {
                return _context.Clients.OrderBy(x => x.Id).ToList();
            }
        }

        /// <summary>
        /// Récupérer un client en particulié via son ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Client? GetClientById(int id)
        {
            return _context.Clients.FirstOrDefault(c => c.Id == id);
        }
        /// <summary>
        /// Permet d'ajouter un client à la base de données et de sauvegarder
        /// </summary>
        /// <param name="client"></param>
        public void Ajouter(Client client)
        {
            _context.Clients.Add(client);
            _context.SaveChanges();
        }
        /// <summary>
        /// Permet la suppression d'un client de la base de données et de sauvegarder
        /// </summary>
        /// <param name="id"></param>
        public void Supprimer(int id)
        {
            Client client = GetClientById(id);
            _context.Clients.Remove(client);
            _context.SaveChanges();
        }
        /// <summary>
        /// Permet la modification d'un client de la base de données et de sauvegarder
        /// </summary>
        /// <param name="client"></param>
        public void Modifier(Client client)
        {
            _context.Clients.Update(client);
            _context.SaveChanges();
        }
        /// <summary>
        /// Requête LINQ qui permet de vérifier si le permis de conduire existe déjà dans la base de données
        /// Utiliser dans le controlleur "Validation"
        /// </summary>
        /// <param name="numeroPermis"></param>
        /// <returns></returns>
        public bool IsPermisExists(string numeroPermis)
        {
            return _context.Clients.Any(c => c.NumeroPermis == numeroPermis);
        }
    }
}
