﻿using Microsoft.EntityFrameworkCore;

namespace LocationVoiturePhilippeB.Models
{
    public class BDVoitureRepository : IVoitureRepository
    {
        private LocationVoitureDbContext _context;

        public BDVoitureRepository(LocationVoitureDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Récupérer les voitures de la base de données sous forme de liste
        /// </summary>
        public List<Voiture> ListeVoitures
        {
            get
            {
                return _context.Voitures.OrderBy(x => x.Id).ToList();
            }
        }
        /// <summary>
        /// Méthode qui permet de trouver une voiture en particulier grâce à son ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Voiture GetVoitureById(int id)
        {
            return _context.Voitures.FirstOrDefault(v => v.Id == id);
        }
        /// <summary>
        /// Méthode qui permet l'ajout d'une voiture dans la base de données
        /// </summary>
        /// <param name="voiture"></param>
        public void Ajouter (Voiture voiture)
        {
            _context.Voitures.Add(voiture);
            _context.SaveChanges();
        }
        /// <summary>
        /// Méthode qui permet la suppression d'une voiture dans la base de données
        /// </summary>
        /// <param name="id"></param>
        public void Supprimer (int id) 
        {
            Voiture voiture = GetVoitureById(id);
            _context.Voitures.Remove(voiture);
            _context.SaveChanges();
        }
        /// <summary>
        /// Méthode qui permet la modification du voiture dans la base de données
        /// </summary>
        /// <param name="voiture"></param>
        public void Modifier(Voiture voiture)
        {
            _context.Voitures.Update(voiture);
            _context.SaveChanges();
        }
        /// <summary>
        /// Requête LINQ qui permet de trier la liste de voitures selon leur nombre de fois louée
        /// - Utiliser dans le ViewModel "PagePrincipale"
        /// </summary>
        /// <returns></returns>
        public List<Voiture> GetVoiturePopulaire()
        {
            return _context.Voitures.OrderByDescending(v => v.NombreFoisLocation).ToList();
        }
    }

}
