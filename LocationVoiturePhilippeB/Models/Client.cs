﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LocationVoiturePhilippeB.Models
{
    public class Client
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DisplayName("Identifiant ")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Prénom ")]
        [Remote("CheckPrenomFormat", "Validation", HttpMethod = "POST", ErrorMessage = "Le prénom doit commencer par une lettre majuscule suivie de 3 à 30 lettres minuscules")]
        public string Prenom { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Nom ")]
        [Remote("CheckNomFormat", "Validation", HttpMethod = "POST", ErrorMessage = "Le nom doit commencer par une lettre majuscule suivie de 3 à 30 lettres minuscules")]
        public string Nom { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Adresse ")]
        public string Adresse { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Genre ")]
        [RegularExpression("^(Homme|Femme|Autre)$", ErrorMessage = "Le genre doit être Homme, Femme ou Autre.")]
        public string Genre { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Numéro de téléphone ")]
        [RegularExpression(@"^\d{3}-\d{3}-\d{4}$", ErrorMessage = "Le numéro de téléphone doit être au format ###-###-####")]
        public string Telephone { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Numéro permis de conduire ")]
        [RegularExpression(@"^[A-Za-z0-9\-]+$", ErrorMessage = "Le numéro de permis de conduire doit contenir uniquement des chiffres, des lettres ou des tirets.")]
        [Remote("PermisExist", "Validation", HttpMethod = "POST", ErrorMessage = "Attention ! Ce numéro de permis de conduire est déjà attribué à un client")]
        public string NumeroPermis { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Photo du client ")]
        [NotMapped]
        public IFormFile? Photo { get; set; }
        [DisplayName("Url photo ")]
        public string? urlPhoto { get; set; }
    }
}
