﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace LocationVoiturePhilippeB.Models
{
    /// <summary>
    /// Classe utiliser comme attribut d'annotation personnalisé
    /// - L'utilisateur doit entrer une des valeurs du regex pour valider son champ
    /// </summary>
    public class CouleurValideCustom : ValidationAttribute
    {
        private static readonly Regex CouleurRegex = new Regex("^(rouge|vert|bleu|jaune|orange|violet|rose|blanc|noir|gris)$", RegexOptions.IgnoreCase);

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                string couleur = value.ToString();

                if (!CouleurRegex.IsMatch(couleur))
                {
                    return new ValidationResult(ErrorMessage ?? "Le champ Couleur doit contenir un nom de couleur valide.");
                }
            }

            return ValidationResult.Success;
        }
    }
}
