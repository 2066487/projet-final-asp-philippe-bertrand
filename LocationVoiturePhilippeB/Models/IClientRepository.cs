﻿namespace LocationVoiturePhilippeB.Models
{
    public interface IClientRepository
    {
        public List<Client> ListeClients { get; }
        public Client? GetClientById(int id);
        public void Ajouter(Client client);
        public void Supprimer(int id);
        public void Modifier(Client client);
        public bool IsPermisExists(string numeroPermis);
    }
}
