﻿using System.Diagnostics;

namespace LocationVoiturePhilippeB.Models
{
    /// <summary>
    /// Interface qui stocke toutes les méthodes utilisées dans le panier d'achat
    /// Appelée et implémentée dans la classe "Panier"
    /// </summary>
    public interface IPanierRepository
    {
        public List<PanierVoiture> VoiturePanier { get; }
        public void SetSessionId(string sessionId);
        public void AjouterAuPanier(Voiture voiture);
        public void SupprimerDuPanier(Voiture voiture);
        public void ViderPanier();
        public decimal MontantTotalPanier();
        public int NombreHeuresTotalDuPanier(string PanierSessionId);
        public List<PanierVoiture> GetLocations();
    }
}
