﻿namespace LocationVoiturePhilippeB.Models
{
    /// <summary>
    /// Interface qui stocke toutes les méthodes utilisées pour la gestion des voitures
    /// Appelée et implémentée dans le "BDVoitureRepository"
    /// </summary>
    public interface IVoitureRepository
    {
        public List<Voiture> ListeVoitures { get; }
        public Voiture? GetVoitureById(int id);
        public void Ajouter(Voiture voiture);
        public void Supprimer(int id);
        public void Modifier(Voiture voiture);
        public List<Voiture> GetVoiturePopulaire();
    }
}
