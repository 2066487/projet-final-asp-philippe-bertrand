﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LocationVoiturePhilippeB.Models
{
    /// <summary>
    /// Classe DBContext qui permet la création des tables dans la base de données
    /// - Voitures
    /// - Clients
    /// - PanierVoitures
    /// - User
    /// - Roles
    /// </summary>
    public class LocationVoitureDbContext  : IdentityDbContext
    {
        public LocationVoitureDbContext(DbContextOptions<LocationVoitureDbContext> options) : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Voiture> Voitures { get; set; }
        public DbSet<PanierVoiture> PanierVoitures { get;set; }

    }
}
