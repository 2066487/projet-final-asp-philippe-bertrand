﻿using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace LocationVoiturePhilippeB.Models
{
    /// <summary>
    /// Repository qui implémente toutes les méthodes de l'interface "IPanierRepository"
    /// </summary>
    public class Panier : IPanierRepository
    {
        // Appel du dbContext
        private readonly LocationVoitureDbContext _context;

        // Attribut pour le id de la session
        public string? PanierSessionId;

        /// <summary>
        /// Retourne l'ensemble de la table PanierVoiture
        /// - On inclue la propriété de navigation si le PanierSessionID est Identique à celui du PanierVoiture
        /// </summary>
        public List<PanierVoiture> VoiturePanier 
        {
            get
            {
                  return _context.PanierVoitures
                .Include(pv => pv.Voiture)
                .Where(pv => pv.PanierSessionId == PanierSessionId)
                .ToList();
            }
        }
        /// <summary>
        /// Constructeur du panier qui contient le context
        /// </summary>
        /// <param name="context"></param>
        public Panier(LocationVoitureDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Définir le session id avec un paramètre entré de type string
        /// </summary>
        /// <param name="sessionId"></param>
        public void SetSessionId(string sessionId)
        {
            PanierSessionId = sessionId;
        }
        /// <summary>
        /// Permet l'ajout d'une voiture dans le panier
        /// - Si la voiture est une nouvelle instance non-disponible dans le panier, on lui crée une instance.
        /// - Sinon, on incrémente le nombre d'heures de location ce qui fait monter le prix.
        /// - Sauvegarder les données dans la BD
        /// </summary>
        /// <param name="voiture"></param>
        public void AjouterAuPanier(Voiture voiture)
        {
            PanierVoiture panierVoiture = _context.PanierVoitures.SingleOrDefault(vp => vp.PanierSessionId == PanierSessionId && vp.VoitureId == voiture.Id);

            if (panierVoiture != null) 
            {
                panierVoiture.DureeLocation++;
            }
            else
            {
                panierVoiture = new PanierVoiture
                {
                    Voiture = voiture,
                    PanierSessionId = PanierSessionId,
                    DureeLocation = 1,
                    VoitureId = voiture.Id
                };

                _context.PanierVoitures.Add(panierVoiture);

            }
            _context.SaveChanges();
        }
        /// <summary>
        /// Permet la suppression d'une voiture dans le panier
        /// - Système similaire à l'ajout sauf que cette fois-ci on décrémente si la voiture a été ajouté plusieurs fois
        /// - On l'a supprime totalement s'il y a juste 1 heure de location sur la voiture
        /// - Sauvegarde des données dans la BD
        /// </summary>
        /// <param name="voiture"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public void SupprimerDuPanier(Voiture voiture)
        {
            PanierVoiture panierVoiture = _context.PanierVoitures.SingleOrDefault(vp => vp.PanierSessionId == PanierSessionId && vp.VoitureId == voiture.Id);

            if (panierVoiture != null)
            {
                if (panierVoiture.DureeLocation > 1)
                {
                    panierVoiture.DureeLocation--;
                }
                else
                {
                    _context.PanierVoitures.Remove(panierVoiture);
                }
            }
            else
            {
                throw new InvalidOperationException("Le véhicule ne fait pas partie du panier.");
            }

            _context.SaveChanges();
        }
        /// <summary>
        /// Méthode qui permet la suppression complète de tous les éléments du panier.
        /// - Sauvegarde des données dans la BD
        /// </summary>
        public void ViderPanier()
        {
            var voiturePanier = _context.PanierVoitures.Where(vp => vp.PanierSessionId == PanierSessionId);

            _context.PanierVoitures.RemoveRange(voiturePanier);

            _context.SaveChanges();
        }
        /// <summary>
        /// Méthode qui permet de calculer la somme totale du panier d'achat avec une requête LINQ
        /// - Retourne un nombre décimal
        /// </summary>
        /// <returns></returns>
        public decimal MontantTotalPanier()
        {
            return _context.PanierVoitures.Where(vp => vp.PanierSessionId == PanierSessionId).Select(vp => vp.Voiture.PrixParHeure * vp.DureeLocation).Sum();
        }
        /// <summary>
        /// Méthode qui permet de calculer le nombre d'heures total du panier avec une requête LINQ
        /// - Retourne un nombre entier
        /// </summary>
        /// <param name="PanierSessionId"></param>
        /// <returns></returns>
        public int NombreHeuresTotalDuPanier(string PanierSessionId)
        {
            return _context.PanierVoitures.Where(vp => vp.PanierSessionId == PanierSessionId).Select(vp => vp.DureeLocation).Sum();
        }
        /// <summary>
        /// Permet de retourner toutes les locations faites sans prendre compte du panierSessionId
        /// - Utilisé pour l'affichage des locations dans le ViewModel
        /// </summary>
        /// <returns></returns>
        public List<PanierVoiture> GetLocations()
        {
            return _context.PanierVoitures
                .Include(pv => pv.Voiture)
                .ToList();
        }
    }
}
