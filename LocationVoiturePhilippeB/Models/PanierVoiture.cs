﻿namespace LocationVoiturePhilippeB.Models
{
    /// <summary>
    /// Classe qui fait une relation entre une voiture et un panier
    /// Elle possède :
    /// - Id, VoitureId, PanierSessionId
    /// - Objet voiture 
    /// - Durée de location (heures)
    /// </summary>
    public class PanierVoiture
    {
        public int Id { get; set; }
        public int VoitureId { get; set; }
        public Voiture Voiture { get; set; }
        public int DureeLocation { get; set; }
        public string PanierSessionId { get; set; }
    }
}
