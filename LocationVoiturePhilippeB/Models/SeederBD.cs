﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace LocationVoiturePhilippeB.Models
{
    /// <summary>
    /// Classe statique qui joue le rôle "d'initialisateur de données" ou "Seeder" pour la base de données
    /// On insère des données initiales pour les tables :
    /// - Client
    /// - Voiture
    /// - User
    /// - Role 
    /// - RoleUser
    /// </summary>
    public static class SeederBD
    {
        public static List<Client> _clients = new List<Client>
        {
            new Client { Prenom = "Philippe", Nom = "Bertrand", Adresse = "5 Avenue Anatole France, 75007 Paris, France", Genre = "Homme", Telephone = "819-445-2345", NumeroPermis = "A9999-999999-99", urlPhoto = "wwwroot/images/PhotoClient/homme1.jpg", },
            new Client { Prenom = "Guillaume", Nom = "Paoli", Adresse = "10 Rue de la Paix, 75002 Paris, France", Genre = "Homme", Telephone = "819-889-5690", NumeroPermis = "B14687-547623-77", urlPhoto = "wwwroot/images/PhotoClient/homme2.jpg" },
            new Client { Prenom = "Sarah", Nom = "Laviolette", Adresse = "Alexanderplatz 1, 10178 Berlin, Allemagne", Genre = "Femme", Telephone = "514-679-2345", NumeroPermis = "D16887-997623-57", urlPhoto = "wwwroot/images/PhotoClient/femme1.jpg" },
            new Client { Prenom = "Amandine", Nom = "Cournoyer", Adresse = "742 Evergreen Terrace, Springfield, USA", Genre = "Femme", Telephone = "235-679-4533", NumeroPermis = "C15670-556789-11", urlPhoto = "wwwroot/images/PhotoClient/femme2.jpg" },
            new Client { Prenom = "Jean", Nom = "Neymar", Adresse = "221B Baker Street, London, Angleterre", Genre = "Homme", Telephone = "819-889-5611", NumeroPermis = "E14687-577623-77", urlPhoto = "wwwroot/images/PhotoClient/homme3.jpg" },
            new Client { Prenom = "Julie", Nom = "Gates", Adresse = "1600 Pennsylvania Avenue NW, Washington, D.C., USA", Genre = "Femme", Telephone = "413-990-6732", NumeroPermis = "F23687-533623-77", urlPhoto = "wwwroot/images/PhotoClient/femme3.jpg" }
        };

        public static List<Voiture> _voitures = new List<Voiture>
        {
            new Voiture { Marque = "Toyota", Modele = "Corolla", Couleur = "Bleu", AnneeFabrication = 2019, CapaciteMax = 5, UrlImage = "wwwroot/images/Vehicule/toyotacorola2019.png", NombreFoisLocation = 11,PrixParHeure = 20},
            new Voiture { Marque = "Honda", Modele = "Civic", Couleur = "Noir", AnneeFabrication = 2020, CapaciteMax = 5, UrlImage = "wwwroot/images/Vehicule/hondacivic2020.jpg", NombreFoisLocation = 15, PrixParHeure = 15 },
            new Voiture { Marque = "Ford", Modele = "Mustang", Couleur = "Rouge", AnneeFabrication = 2022, CapaciteMax = 2, UrlImage = "wwwroot/images/Vehicule/fordmustang2022jpg.jpg", NombreFoisLocation = 3, PrixParHeure = 40 },
            new Voiture { Marque = "Volkswagen", Modele = "Golf", Couleur = "Blanc", AnneeFabrication = 2018, CapaciteMax = 5, UrlImage = "wwwroot/images/Vehicule/volkswagengolf2018.jpg",NombreFoisLocation = 23, PrixParHeure = 10 },
            new Voiture { Marque = "BMW", Modele = "Série 3", Couleur = "Gris", AnneeFabrication = 2021, CapaciteMax = 4, UrlImage = "wwwroot/images/Vehicule/bmwserie32018.jpg", NombreFoisLocation = 10, PrixParHeure = 50 },
            new Voiture { Marque = "Mercedes-Benz", Modele = "Classe C", Couleur = "Argent", AnneeFabrication = 2020, CapaciteMax = 4, UrlImage = "wwwroot/images/Vehicule/mercedesclassec2020.jpg", NombreFoisLocation = 12, PrixParHeure = 45 },
            new Voiture { Marque = "Audi", Modele = "A4", Couleur = "Noir", AnneeFabrication = 2019, CapaciteMax = 4, UrlImage = "wwwroot/images/Vehicule/audia42018.jpg", NombreFoisLocation = 9, PrixParHeure = 25 },
            new Voiture { Marque = "Chevrolet", Modele = "Camaro", Couleur = "Jaune", AnneeFabrication = 2022, CapaciteMax = 2, UrlImage = "wwwroot/images/Vehicule/camaro.jpg", NombreFoisLocation = 9, PrixParHeure = 35 },
            new Voiture { Marque = "Nissan", Modele = "Rogue", Couleur = "Gris", AnneeFabrication = 2021, CapaciteMax = 5, UrlImage ="wwwroot/images/Vehicule/nissarogue2021.jpg",NombreFoisLocation = 14, PrixParHeure = 15},
            new Voiture { Marque = "Hyundai", Modele = "Elantra", Couleur = "Bleu", AnneeFabrication = 2020, CapaciteMax = 5, UrlImage = "wwwroot/images/Vehicule/hyundaielantra2020.jpg", NombreFoisLocation = 34, PrixParHeure = 20 }
        };

        public static List<IdentityRole> _roles = new List<IdentityRole>
        {
            new IdentityRole { Name = "Admin", Id = "afe97041-d66c-4c27-bb65-8db1e91761a3" },
            new IdentityRole { Name = "Client", Id = "5bd8579b-a565-436e-b3d3-7f2907a46e8f" }
        };
        public static List<IdentityUser> _users = new List<IdentityUser>
        {
            new IdentityUser
            {
                Id = "2fc2f58e-0ab6-4a82-ab11-03e85c1e6bac",
                UserName = "ADMIN",
				NormalizedUserName = "PHILIPPE00471@GMAIL.COM",
                Email = "philippe00471@gmail.com",
                NormalizedEmail = "PHILIPPE00471@GMAIL.COM",
                EmailConfirmed = true,
                PasswordHash = "AQAAAAEAACcQAAAAENPpZBwfwJd26xRDtbnuu6VOgXqvQTyQL+4Q52ilvg9m/tPSVxdw+Bmdyvos5xIk6w==", // Androxus123!
				SecurityStamp = "BTNHI2PHZBE6KIRSHYRALU6URXQAVBGP",
                ConcurrencyStamp = "99a78ea0-417d-43b0-a173-39ce9dccb5e4",
                PhoneNumberConfirmed = false,
                TwoFactorEnabled = false,
                LockoutEnabled = false,
                AccessFailedCount = 0
            },
            /*new IdentityUser
            {
                Id = "56daeb27-ee34-460c-9ea8-9dec9b181f3e",
                UserName = "Client ", 
                NormalizedUserName = "CLIENT",
                Email = "client00471@gmail.com",
                NormalizedEmail = "CLIENT00471@GMAIL.COM",
                EmailConfirmed = true,
                PasswordHash = "AQAAAAEAACcQAAAAEAs931/OX1pEcU3uDP2PecoVcboKHpHnZC88ztfzc2VAUYlSB54ZxpayqRH8KkwH5Q==", // Androxus123!
                SecurityStamp = "C2RGVBKJOTCDSVHFBJVZ2XHSK3DWNQPW",
                ConcurrencyStamp = "32ad47dd-76e2-4c32-997d-12c1b0c4204b",
                PhoneNumberConfirmed = false,
                TwoFactorEnabled = false,
                LockoutEnabled = false,
                AccessFailedCount = 0
            }*/
        };
        public static List<IdentityUserRole<string>> _userRole = new List<IdentityUserRole<string>>
        {
            new IdentityUserRole<string> // Compte admin 
			{
                RoleId = "afe97041-d66c-4c27-bb65-8db1e91761a3",
                UserId = "2fc2f58e-0ab6-4a82-ab11-03e85c1e6bac"
            },
           /* new IdentityUserRole<string> // Compte client 
			{
                RoleId = "5bd8579b-a565-436e-b3d3-7f2907a46e8f",
                UserId = "56daeb27-ee34-460c-9ea8-9dec9b181f3e"
            }*/
        };


        /// <summary>
        /// Méthode statique qui prend les listes créées plus haut et les converties dans les tables de la base de données
        /// - Elle se réfère à la base DbContext pour trouvées les tables
        /// - La méthode est appelé juste avant le démarrage de l'application
        /// </summary>
        /// <param name="applicationBuilder"></param>
        public static void Seed(IApplicationBuilder applicationBuilder)
        {
            LocationVoitureDbContext locationVoitureDbContext = applicationBuilder.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<LocationVoitureDbContext>();

            if (!locationVoitureDbContext.Clients.Any())
            {
                locationVoitureDbContext.Clients.AddRange(_clients);
            }
            locationVoitureDbContext.SaveChanges();

            if (!locationVoitureDbContext.Voitures.Any())
            {
                locationVoitureDbContext.Voitures.AddRange(_voitures);
            }
            locationVoitureDbContext.SaveChanges();

            if (!locationVoitureDbContext.Roles.Any())
            {
                locationVoitureDbContext.Roles.AddRange(_roles);
            }
            locationVoitureDbContext.SaveChanges();

            if (!locationVoitureDbContext.Users.Any())
            {
                locationVoitureDbContext.Users.AddRange(_users);
            }
            locationVoitureDbContext.SaveChanges();

            if (!locationVoitureDbContext.UserRoles.Any())
            {
                locationVoitureDbContext.UserRoles.AddRange(_userRole);
            }
            locationVoitureDbContext.SaveChanges();
        }
    }
}
