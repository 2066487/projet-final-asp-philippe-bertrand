﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;

namespace LocationVoiturePhilippeB.Models
{
    /// <summary>
    /// Classe voiture avec tous ses attributs
    /// </summary>
    public class Voiture
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Marque ")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Le nom de la marque doit avoir entre 2 et 50 caractères.")]
        public string Marque { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Modèle ")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Le modèle de la voiture doit avoir entre 2 et 50 caractères!")]
        public string Modele { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Couleur ")]
        [CouleurValideCustom]
        public string Couleur { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Année de fabrication")]
        [Remote("ValiderAnneeFabrication", "Validation", HttpMethod = "POST")]
        public int AnneeFabrication { get; set; }
        public int? ClientId { get; set; }
        public Client? client { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Capacité maximale")]
        [Range(1,9,ErrorMessage = "La capacité de passagers maximale ne peut pas dépasser 9 personnes.")]
        public int CapaciteMax { get; set; }
        [NotMapped]
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Image ")]
        public IFormFile? Image { get; set; }
        public string? UrlImage { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Nombre de fois louée")]
        public int NombreFoisLocation { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Prix par heure ")]
        [Range(0, 100, ErrorMessage = "Le prix par heure doit être compris entre 0 et 100.")]
        public decimal PrixParHeure { get; set; }

    }
}
