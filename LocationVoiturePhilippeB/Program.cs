using Microsoft.EntityFrameworkCore;
using LocationVoiturePhilippeB.Models;
using LocationVoiturePhilippeB.ViewModels;
using Microsoft.AspNetCore.Identity;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("LocationVoitureDbContext") ?? throw new InvalidOperationException("Connection string 'LocationVoitureDbContextConnection' not found.");

builder.Services.AddControllersWithViews();

builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(3); 
});

builder.Services.AddScoped<IClientRepository, BDClientRepository>();
builder.Services.AddScoped<IVoitureRepository, BDVoitureRepository>();
builder.Services.AddScoped<IPanierRepository, Panier>();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.AddScoped<PagePrincipaleViewModel>();
builder.Services.AddScoped<LocationViewModel>();

builder.Services.AddDbContext<LocationVoitureDbContext>(Options =>
{
    Options.UseSqlServer(builder.Configuration["ConnectionStrings:LocationVoitureDbContext"]);
});


builder.Services.AddIdentity<IdentityUser, IdentityRole>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddEntityFrameworkStores<LocationVoitureDbContext>().AddDefaultUI().AddDefaultTokenProviders();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Erreurs/GererErreurGlobale");
    app.UseStatusCodePagesWithReExecute("/Erreurs/GererErreur{0}");
}
app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseAuthentication();
app.UseAuthorization();

app.UseSession();

app.MapControllerRoute(
 name: "default",
 pattern: "{controller=PagePrincipale}/{action=Index}"
);

app.MapRazorPages();

SeederBD.Seed(app);

app.Run();
