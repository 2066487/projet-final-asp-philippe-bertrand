﻿namespace LocationVoiturePhilippeB.ViewModels
{
    public class ErreurViewModel
    {
        public int CodeErreur { get; set; }
        public string ErrorMsg { get; set; }
        public string CheminException { get; set; }
        public string MessageException { get; set; }
        public string TraceException { get; set; }
        public bool ModeDev { get; set; }
    }
}
