﻿using LocationVoiturePhilippeB.Models;

namespace LocationVoiturePhilippeB.ViewModels
{
    public class LocationViewModel
    {
        private readonly IPanierRepository _locations;
        public List<PanierVoiture> ListeLocations { get; set; }
        public decimal? MontantTotal { get; set; }
        public DateTime? Datedebut { get; set; }

        public LocationViewModel()
        {
            
        } 
    }
}
