﻿using LocationVoiturePhilippeB.Models;

namespace LocationVoiturePhilippeB.ViewModels
{
    public class PagePrincipaleViewModel
    {
        private readonly IVoitureRepository _voitureRepository;

        public List<Voiture> VoituresPopulaire { get; set; }

        public PagePrincipaleViewModel(IVoitureRepository voitureRepository)
        {
            _voitureRepository = voitureRepository;

            VoituresPopulaire = _voitureRepository.GetVoiturePopulaire();
        }
    }
}
