﻿using LocationVoiturePhilippeB.Models;

namespace LocationVoiturePhilippeB.ViewModels
{
    public class PanierViewModel
    {
        public List<PanierVoiture> ListeVoituresPanier { get; set; }
        public decimal MontantTotal { get; set; }
    }
}
