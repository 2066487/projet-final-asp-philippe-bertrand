﻿using Microsoft.AspNetCore.Identity;

namespace LocationVoiturePhilippeB.ViewModels
{
    public class RoleMembresViewModel
    {
        public IdentityRole Role { get; set; }
        public List<IdentityUser> NonMembres { get; set; }
        public IList<IdentityUser> Membres { get; set; }
    }
}
