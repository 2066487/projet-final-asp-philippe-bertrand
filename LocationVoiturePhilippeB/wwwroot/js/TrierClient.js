﻿$(document).ready(function () {
    const button = $("#boutonTri");
    const select = $("#selectTri");

    button.click(function (e) {
        e.preventDefault();

        const ordre = select.val().trim();

        $.ajax({
            type: "POST",
            url: "/api/Trier",
            data: { ordre: ordre },
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            dataType: "json",
            success: function (data) {
                const conteneurClients = $('#conteneur-clients');
                conteneurClients.empty();

                data.forEach(function (client) {
                    const urlPhoto = client.urlPhoto;
                    const prenom = client.prenom;
                    const nom = client.nom;
                    const adresse = client.adresse;
                    const telephone = client.telephone;

                    const card = $('<div class="col-md-4">' +
                        '<div class="card mb-4">' +
                        '<img src="' + urlPhoto + '" class="card-img-top" alt="Photo du client">' +
                        '<div class="card-body">' +
                        '<h5 class="card-title">' + prenom + ' ' + nom + '</h5>' +
                        '<p class="card-text">Adresse: ' + adresse + '</p>' +
                        '<p class="card-text">Téléphone: ' + telephone + '</p>' +
                        '<hr>' +
                        '<div>' +
                        '<a class="btn btn-outline-success card text-center" data-toggle="modal" data-target="#modalInfosCompletes_' + client.id + '">Informations complètes</a>' +
                        '<div class="d-flex justify-content-center">' +
                        '<a class="btn btn-outline-danger mt-3 card text-center" data-toggle="modal" data-target="#modalSupprimer_' + client.id + '">Supprimer</a>' +
                        '<a class="btn btn-outline-warning mt-3 card text-center" asp-controller="Client" asp-action="Modifier" asp-route-id=' + client.id + '>Modifier</a>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>');

                    conteneurClients.append(card);
                });
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    });
});