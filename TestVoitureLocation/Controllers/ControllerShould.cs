﻿using LocationVoiturePhilippeB.Controllers;
using LocationVoiturePhilippeB.Controllers.Validation;
using LocationVoiturePhilippeB.Models;
using LocationVoiturePhilippeB.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestVoitureLocation.Controllers
{
    public class ControllerShould
    {
        private IWebHostEnvironment _webHostEnvironment = new MockWebHostEnvironment();

        [Fact]
        public void TestCreerClient()
        {
            DbContextOptions<LocationVoitureDbContext> dbContextOptions = new DbContextOptionsBuilder<LocationVoitureDbContext>()
                .UseInMemoryDatabase("LocationDbTestCreerClient").Options;

            var _dbcontext = new LocationVoitureDbContext(dbContextOptions);

            _dbcontext.Clients.AddRange(
                new Client { Prenom = "Philippe", Nom = "Bertrand", Adresse = "5 Avenue Anatole France, 75007 Paris, France", Genre = "Homme", Telephone = "819-445-2345", NumeroPermis = "A9999-999999-99", urlPhoto = "wwwroot/images/PhotoClient/homme1.jpg", },
                new Client { Prenom = "Guillaume", Nom = "Paoli", Adresse = "10 Rue de la Paix, 75002 Paris, France", Genre = "Homme", Telephone = "819-889-5690", NumeroPermis = "B14687-547623-77", urlPhoto = "wwwroot/images/PhotoClient/homme2.jpg" },
                new Client { Prenom = "Sarah", Nom = "Laviolette", Adresse = "Alexanderplatz 1, 10178 Berlin, Allemagne", Genre = "Femme", Telephone = "514-679-2345", NumeroPermis = "D16887-997623-57", urlPhoto = "wwwroot/images/PhotoClient/femme1.jpg" }
            );

            _dbcontext.SaveChanges();

            Client nouveauClient = new Client { Prenom = "Amandine", Nom = "Cournoyer", Adresse = "742 Evergreen Terrace, Springfield, USA", Genre = "Femme", Telephone = "235-679-4533", NumeroPermis = "C15670-556789-11", urlPhoto = "wwwroot/images/PhotoClient/femme2.jpg" };

            BDClientRepository _clientRepository = new BDClientRepository(_dbcontext);

            ClientController clientController = new ClientController(_clientRepository, _webHostEnvironment);
            
            Assert.Equal(3, _dbcontext.Clients.Count());

            var resultat = clientController.AjouterClient(nouveauClient);

            // Assertions
            Assert.Equal(4, _dbcontext.Clients.Count());
            Assert.Contains(nouveauClient, _dbcontext.Clients);
            Assert.Contains(_dbcontext.Clients, c => c.Id == 4);
            var redirection = Assert.IsType<RedirectToActionResult>(resultat);
            Assert.Equal("ListeClients", redirection.ActionName);
        }
        [Theory]
        [InlineData(1)]
        public void TestSupprimerClient(int clientId)
        {
            DbContextOptions<LocationVoitureDbContext> dbContextOptions = new DbContextOptionsBuilder<LocationVoitureDbContext>()
                .UseInMemoryDatabase("LocationDbTestSupprimerClient").Options;

            var _dbcontext = new LocationVoitureDbContext(dbContextOptions);

            _dbcontext.Clients.AddRange(
                new Client { Prenom = "Philippe", Nom = "Bertrand", Adresse = "5 Avenue Anatole France, 75007 Paris, France", Genre = "Homme", Telephone = "819-445-2345", NumeroPermis = "A9999-999999-99", urlPhoto = "wwwroot/images/PhotoClient/homme1.jpg", },
                new Client { Prenom = "Guillaume", Nom = "Paoli", Adresse = "10 Rue de la Paix, 75002 Paris, France", Genre = "Homme", Telephone = "819-889-5690", NumeroPermis = "B14687-547623-77", urlPhoto = "wwwroot/images/PhotoClient/homme2.jpg" },
                new Client { Prenom = "Sarah", Nom = "Laviolette", Adresse = "Alexanderplatz 1, 10178 Berlin, Allemagne", Genre = "Femme", Telephone = "514-679-2345", NumeroPermis = "D16887-997623-57", urlPhoto = "wwwroot/images/PhotoClient/femme1.jpg" }
            );

            _dbcontext.SaveChanges();

            BDClientRepository _clientRepository = new BDClientRepository(_dbcontext);

            ClientController clientController = new ClientController(_clientRepository, _webHostEnvironment);

            Assert.Equal(3, _dbcontext.Clients.Count());

            Client clientSupprimer = _clientRepository.ListeClients.FirstOrDefault(c => c.Id == 1);
            var resultat = clientController.Supprimer(clientId);

            // Assertions 
            Assert.NotNull(_dbcontext.Clients);

            Assert.Equal(2, _dbcontext.Clients.Count());

            var redirectResult = Assert.IsType<RedirectToActionResult>(resultat);

            Assert.Equal("ListeClients", redirectResult.ActionName);

            Assert.DoesNotContain<Client>(clientSupprimer, _clientRepository.ListeClients);

            Assert.Null(_clientRepository.ListeClients.FirstOrDefault(c => c.Id == 1));
        }
        [Theory]
        [InlineData("Catherine", "Tremblay", "Femme")]
        public void TestModifierClient(string prenom, string nom, string genre)
        {
            DbContextOptions<LocationVoitureDbContext> dbContextOptions = new DbContextOptionsBuilder<LocationVoitureDbContext>()
                .UseInMemoryDatabase("LocationDbTestModifierClient").Options;

            var _dbcontext = new LocationVoitureDbContext(dbContextOptions);

            _dbcontext.Clients.AddRange(
                new Client { Prenom = "Philippe", Nom = "Bertrand", Adresse = "5 Avenue Anatole France, 75007 Paris, France", Genre = "Homme", Telephone = "819-445-2345", NumeroPermis = "A9999-999999-99", urlPhoto = "wwwroot/images/PhotoClient/homme1.jpg" },
                new Client { Prenom = "Guillaume", Nom = "Paoli", Adresse = "10 Rue de la Paix, 75002 Paris, France", Genre = "Homme", Telephone = "819-889-5690", NumeroPermis = "B14687-547623-77", urlPhoto = "wwwroot/images/PhotoClient/homme2.jpg" },
                new Client { Prenom = "Sarah", Nom = "Laviolette", Adresse = "Alexanderplatz 1, 10178 Berlin, Allemagne", Genre = "Femme", Telephone = "514-679-2345", NumeroPermis = "D16887-997623-57", urlPhoto = "wwwroot/images/PhotoClient/femme1.jpg" }
            );

            _dbcontext.SaveChanges();

            BDClientRepository _clientRepository = new BDClientRepository(_dbcontext);

            ClientController clientController = new ClientController(_clientRepository, _webHostEnvironment);

            Client clientModifier = _clientRepository.ListeClients.FirstOrDefault(c => c.Id == 1);
            clientModifier.Prenom = prenom;
            clientModifier.Nom = nom;
            clientModifier.Genre = genre;

            // Act
            var resultat = clientController.ModifierClient(clientModifier);

            // Assert
            Assert.Equal(prenom, _clientRepository.ListeClients.FirstOrDefault(c => c.Id == 1)?.Prenom);

            var redirectResult = Assert.IsType<RedirectToActionResult>(resultat);
            Assert.Equal("ListeClients", redirectResult.ActionName);

            Assert.Equal(3, _dbcontext.Clients.Count());

            Assert.Contains(_dbcontext.Clients, c => c.Nom == nom);

            Assert.DoesNotContain(_dbcontext.Clients, c => c.Prenom == "Philippe");
        }
        /// <summary>
        /// Test pour la validation du champ des prénoms dans le formulaire
        /// </summary>
        [Fact]
        public void TestCheckPrenomFormat()
        {
            DbContextOptions<LocationVoitureDbContext> dbContextOptions = new DbContextOptionsBuilder<LocationVoitureDbContext>()
               .UseInMemoryDatabase("LocationDbTestModifierClient").Options;

            var _dbcontext = new LocationVoitureDbContext(dbContextOptions);

            BDClientRepository _clientRepository = new BDClientRepository(_dbcontext);
            
            var controller = new ValidationController(_clientRepository);

            var result1 = controller.CheckPrenomFormat("John");
            var result2 = controller.CheckPrenomFormat("john");
            var result3 = controller.CheckPrenomFormat("John123");
            var result4 = controller.CheckPrenomFormat("Jo");

            // Assert
            Assert.True((bool)result1.Value);
            Assert.False((bool)result2.Value);
            Assert.False((bool)result3.Value);
            Assert.False((bool)result4.Value);
        }
        /// <summary>
        /// Test qui permet de vérifier le bon fonctionnement de la méthode d'action PermisExist
        /// L'utilisation de la classe Mock permet d'éviter les dépendances reliées au Repository
        /// </summary>
        [Fact]
        public void TestPermisExist()
        {         
            string numeroPermisExisting = "ABC123";
            string numeroPermisNonExisting = "XYZ456";
            var clientRepository = new Mock<IClientRepository>();
            clientRepository.Setup(c => c.IsPermisExists(numeroPermisExisting)).Returns(true);
            clientRepository.Setup(c => c.IsPermisExists(numeroPermisNonExisting)).Returns(false);

            var controller = new ValidationController(clientRepository.Object);

            
            var resultatValide = controller.PermisExist(numeroPermisExisting);
            var resulatInvalide = controller.PermisExist(numeroPermisNonExisting);

            // Assertions
            Assert.Equal($"Le numéro de permis {numeroPermisExisting} existe déjà.", (string)resultatValide.Value);
            Assert.True((bool)resulatInvalide.Value);
        }
        [Fact]
        public void TestGererErreur()
        {
            var controller = new ErreursController();

            var resultat = controller.GererErreur(404);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(resultat);
            Assert.Equal("Erreur", viewResult.ViewName);

            var viewModel = Assert.IsType<ErreurViewModel>(viewResult.Model);
            Assert.Equal(404, viewModel.CodeErreur);
            Assert.Equal("Désolé, cette page n'existe pas", viewModel.ErrorMsg);
        }

    }
}
