﻿using LocationVoiturePhilippeB.Controllers;
using LocationVoiturePhilippeB.Models;
using LocationVoiturePhilippeB.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestVoitureLocation.Models
{
    public class ModelShould
    {
        [Fact]
        public void CouleurValide()
        {
            var validationAttribute = new CouleurValideCustom();

            var couleurValide = new ValidationContext(new { Couleur = "rouge" });
            var couleurInvalide = new ValidationContext(new { Couleur = "mauve" });

            var validationResultatValide = validationAttribute.GetValidationResult("rouge", couleurValide);
            var validationResultatInvalide = validationAttribute.GetValidationResult("mauve", couleurInvalide);

            // Assertion
            Assert.Equal(ValidationResult.Success, validationResultatValide);
            Assert.NotEqual(ValidationResult.Success, validationResultatInvalide);
        }
        [Fact]
        public void TestGetVoiturePopulaire()
        {
            DbContextOptions<LocationVoitureDbContext> dbContextOptions = new DbContextOptionsBuilder<LocationVoitureDbContext>()
                .UseInMemoryDatabase("LocationDbTestVoituresPopulaire").Options;

            var _dbcontext = new LocationVoitureDbContext(dbContextOptions);

            _dbcontext.Voitures.AddRange(
                new Voiture { Marque = "Toyota", Modele = "Corolla", Couleur = "Bleu", AnneeFabrication = 2019, CapaciteMax = 5, UrlImage = "wwwroot/images/Vehicule/toyotacorola2019.png", NombreFoisLocation = 11, PrixParHeure = 20 },
                new Voiture { Marque = "Honda", Modele = "Civic", Couleur = "Noir", AnneeFabrication = 2020, CapaciteMax = 5, UrlImage = "wwwroot/images/Vehicule/hondacivic2020.jpg", NombreFoisLocation = 15, PrixParHeure = 15 },
                new Voiture { Marque = "Ford", Modele = "Mustang", Couleur = "Rouge", AnneeFabrication = 2022, CapaciteMax = 2, UrlImage = "wwwroot/images/Vehicule/fordmustang2022jpg.jpg", NombreFoisLocation = 3, PrixParHeure = 40 },
                new Voiture { Marque = "Volkswagen", Modele = "Golf", Couleur = "Blanc", AnneeFabrication = 2018, CapaciteMax = 5, UrlImage = "wwwroot/images/Vehicule/volkswagengolf2018.jpg", NombreFoisLocation = 23, PrixParHeure = 10 },
                new Voiture { Marque = "BMW", Modele = "Série 3", Couleur = "Gris", AnneeFabrication = 2021, CapaciteMax = 4, UrlImage = "wwwroot/images/Vehicule/bmwserie32018.jpg", NombreFoisLocation = 10, PrixParHeure = 50 },
                new Voiture { Marque = "Mercedes-Benz", Modele = "Classe C", Couleur = "Argent", AnneeFabrication = 2020, CapaciteMax = 4, UrlImage = "wwwroot/images/Vehicule/mercedesclassec2020.jpg", NombreFoisLocation = 12, PrixParHeure = 45 }
            );

            _dbcontext.SaveChanges();

            var voitureRepository = new BDVoitureRepository(_dbcontext);

            var voituresPopulaires = voitureRepository.GetVoiturePopulaire();

            Assert.NotNull(_dbcontext.Voitures);
            Assert.Equal(6, voituresPopulaires.Count);

            // Vérifie l'ordre renvoyée de la liste des voitures selon leur popularité
            Assert.Equal("Volkswagen", voituresPopulaires[0].Marque);
            Assert.Equal("Honda", voituresPopulaires[1].Marque);
            Assert.Equal("Mercedes-Benz", voituresPopulaires[2].Marque);
            Assert.Equal("Toyota", voituresPopulaires[3].Marque);
            Assert.Equal("BMW", voituresPopulaires[4].Marque);
            Assert.Equal("Ford", voituresPopulaires[5].Marque);
        }
        [Fact]
        public void TestGetVoitureById()
        {
            DbContextOptions<LocationVoitureDbContext> dbContextOptions = new DbContextOptionsBuilder<LocationVoitureDbContext>()
               .UseInMemoryDatabase("LocationDbTestGetVoitureById").Options;

            var _dbcontext = new LocationVoitureDbContext(dbContextOptions);
            
            _dbcontext.Voitures.AddRange(
                new Voiture { Id = 1, Marque = "Toyota", Modele = "Corolla", Couleur = "Bleu", AnneeFabrication = 2019 },
                new Voiture { Id = 2, Marque = "Honda", Modele = "Civic", Couleur = "Rouge", AnneeFabrication = 2020 },
                new Voiture { Id = 3, Marque = "Ford", Modele = "Mustang", Couleur = "Noir", AnneeFabrication = 2021 }
            );
            _dbcontext.SaveChanges();

            var repository = new BDVoitureRepository(_dbcontext);

            var voiture = repository.GetVoitureById(2);

            // Assertion
            Assert.NotNull(voiture);
            Assert.Equal(2, voiture.Id);
            Assert.Equal("Honda", voiture.Marque);
            Assert.Equal("Civic", voiture.Modele);
            Assert.Equal("Rouge", voiture.Couleur);
            Assert.Equal(2020, voiture.AnneeFabrication);
            
        }
        [Fact]
        public void TestSupprimerVoiture()
        {
            DbContextOptions<LocationVoitureDbContext> dbContextOptions = new DbContextOptionsBuilder<LocationVoitureDbContext>()
                .UseInMemoryDatabase("LocationDbTestVoituresSupprimées").Options;

            var _dbcontext = new LocationVoitureDbContext(dbContextOptions);

            _dbcontext.Voitures.AddRange(
                new Voiture { Marque = "Toyota", Modele = "Corolla", Couleur = "Bleu", AnneeFabrication = 2019, CapaciteMax = 5, UrlImage = "wwwroot/images/Vehicule/toyotacorola2019.png", NombreFoisLocation = 11, PrixParHeure = 20 },
                new Voiture { Marque = "Honda", Modele = "Civic", Couleur = "Noir", AnneeFabrication = 2020, CapaciteMax = 5, UrlImage = "wwwroot/images/Vehicule/hondacivic2020.jpg", NombreFoisLocation = 15, PrixParHeure = 15 },
                new Voiture { Marque = "Ford", Modele = "Mustang", Couleur = "Rouge", AnneeFabrication = 2022, CapaciteMax = 2, UrlImage = "wwwroot/images/Vehicule/fordmustang2022jpg.jpg", NombreFoisLocation = 3, PrixParHeure = 40 },
                new Voiture { Marque = "Volkswagen", Modele = "Golf", Couleur = "Blanc", AnneeFabrication = 2018, CapaciteMax = 5, UrlImage = "wwwroot/images/Vehicule/volkswagengolf2018.jpg", NombreFoisLocation = 23, PrixParHeure = 10 },
                new Voiture { Marque = "BMW", Modele = "Série 3", Couleur = "Gris", AnneeFabrication = 2021, CapaciteMax = 4, UrlImage = "wwwroot/images/Vehicule/bmwserie32018.jpg", NombreFoisLocation = 10, PrixParHeure = 50 },
                new Voiture { Marque = "Mercedes-Benz", Modele = "Classe C", Couleur = "Argent", AnneeFabrication = 2020, CapaciteMax = 4, UrlImage = "wwwroot/images/Vehicule/mercedesclassec2020.jpg", NombreFoisLocation = 12, PrixParHeure = 45 }
            );

            _dbcontext.SaveChanges();

            var voitureRepository = new BDVoitureRepository(_dbcontext);
            Assert.Equal(6, _dbcontext.Voitures.Count());

            Voiture voitureSupprimer = voitureRepository.ListeVoitures.FirstOrDefault(v => v.Id == 1);

            voitureRepository.Supprimer(1);

            Assert.DoesNotContain<Voiture>(voitureSupprimer, _dbcontext.Voitures);

            Assert.NotEmpty(_dbcontext.Voitures);

            Assert.Equal(5, _dbcontext.Voitures.Count());

            Assert.Null(_dbcontext.Voitures.FirstOrDefault(v => v.Id == 1));
        }
        [Theory]
        [InlineData("Toyota", "Corolla", "Bleu", 2019, 5, "wwwroot/images/Vehicule/toyotacorola2019.png", 11, 20)]
        public void TestAjouterVoiture(string marque, string modele, string couleur, int anneeFabrication, int capaciteMax, string urlImage, int nombreFoisLocation, decimal prixParHeure)
        {
            DbContextOptions<LocationVoitureDbContext> dbContextOptions = new DbContextOptionsBuilder<LocationVoitureDbContext>()
                .UseInMemoryDatabase("LocationDbTestAjouterVoiture").Options;

            var _context = new LocationVoitureDbContext(dbContextOptions);
            var _voitureRepository = new BDVoitureRepository(_context);

            Voiture nouvelleVoiture = new Voiture
            {
                Marque = marque,
                Modele = modele,
                Couleur = couleur,
                AnneeFabrication = anneeFabrication,
                CapaciteMax = capaciteMax,
                UrlImage = urlImage,
                NombreFoisLocation = nombreFoisLocation,
                PrixParHeure = prixParHeure
            };

            _voitureRepository.Ajouter(nouvelleVoiture);
            var voitureAjoutee = _context.Voitures.First();
            
            // Assertions
            Assert.Single(_context.Voitures);
            
            Assert.Equal(marque, voitureAjoutee.Marque);

            Assert.Equal(anneeFabrication, voitureAjoutee.AnneeFabrication);

            Assert.Equal(1, _context.Voitures.Count());

            Assert.NotEmpty(_context.Voitures);

            Assert.Contains(nouvelleVoiture, _context.Voitures);
        }
    }
}
